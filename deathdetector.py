import settings

class DeathDetector():
    settings = None
    callback = None
    game_name = None
    def __init__(self, game_name, callback, default_data):
        self.callback = callback
        self.game_name = game_name
        self.settings = settings.Settings(game_name,'deathdetector',default_data)

    async def death(self):
        await self.callback(self.game_name)

    async def run(self):
        return