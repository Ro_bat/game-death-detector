#!/bin/python
import appdirs
import os
import yaml
from types import SimpleNamespace

class Settings:
    data = None
    name = None
    appname = None
    def __init__(self, name, appname, default_data):
        data = {}
        self.name = name
        self.appname = appname
        self.load_data(default_data)
        return

    def load_data(self, default_data):
        dir = appdirs.user_config_dir(self.appname)
        filepath = os.path.join(dir,self.name+'.yaml')
        if os.path.isfile(filepath):
            with open(filepath) as f:
                self.data = yaml.load(f, Loader=yaml.FullLoader)
                f.close()
            changed = False
            if self.data is None:
                self.data = {}
                changed = True
            for name, value in default_data.items():
                if name not in self.data:
                    self.data[name] = value
                    changed = True
            if changed:
                self.save_data()
                
        else:
            self.data = default_data
            self.save_data()

    def save_data(self):
        dir = appdirs.user_config_dir(self.appname)
        filepath = os.path.join(dir,self.name+'.yaml')
        os.makedirs(dir, exist_ok=True)
        with open(filepath, 'w') as f:
            yaml.dump(self.data, f)
            f.close()

