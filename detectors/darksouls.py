import numpy as np
import cv2
import time
import asyncio

from deathdetector import DeathDetector

class DarkSoulsDeathDetector(DeathDetector):
    reference_image = None
    y_start = None
    x_start = None 
    def __init__(self,callback):
        default_data = {'score_threshold':10,'sleep_time':7,'video_device':'/dev/video0'}
        super().__init__('Dark Souls',callback,default_data)
        self.reference_image = self.convert(cv2.imread('resources/youdied_cropped.png'))#youdied.png
        self.y_start = 336#300
        self.x_start = 344#300
    def convert(self,frame):
        return frame[:,:,2]-np.mean(frame[:,:,0:2],axis=2)
    def score_image(self,image):
        image_cropped = image[self.y_start:self.y_start+self.reference_image.shape[0],self.x_start:self.x_start+self.reference_image.shape[1],:]
        image_cropped = self.convert(image_cropped)
        diff = np.abs(image_cropped - self.reference_image)
        return np.mean(diff)

    async def run(self):
        video_capture = cv2.VideoCapture(self.settings.data['video_device'])
        while True:
            await asyncio.sleep(0.03)
            ret, frame = video_capture.read()
            if frame is None:
                continue
            score = self.score_image(frame)
            if score < self.settings.data['score_threshold']:
                await asyncio.sleep(self.settings.data['sleep_time'])
                await self.death()
        return
