import asyncio
import deathdetector

from detectors.darksouls import DarkSoulsDeathDetector

async def death_function(name):
    print('Death detected for game {}'.format(name))

def main():
    detector = DarkSoulsDeathDetector(death_function)
    asyncio.get_event_loop().run_until_complete(detector.run())
    return

if __name__=='__main__':
    main()